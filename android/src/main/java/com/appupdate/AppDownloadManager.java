package com.appupdate;

import android.app.Activity;
import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.content.FileProvider;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.hjq.permissions.OnPermission;
import com.hjq.permissions.Permission;
import com.hjq.permissions.XXPermissions;

import java.io.File;
import java.lang.ref.WeakReference;
import java.util.List;

import static com.appupdate.Constants.downloadCancel;

/**
 * @author weifu.zhang
 */
public class AppDownloadManager {

	public static final String TAG = "AppDownloadManager";
	private WeakReference<Activity> weakReference;
	private DownloadManager mDownloadManager;
	private DownloadChangeObserver mDownLoadChangeObserver;
	private DownloadReceiver mDownloadReceiver;
	private long mReqId;
	private OnUpdateListener mUpdateListener;

	private static final String DOWNLOAD_FILE_NAME = "my_app.apk";
	File apkFile;

	String pathDir;
	AppUpdateDialog.UpdateCallback callback;

	public AppDownloadManager(Activity activity, AppUpdateDialog.UpdateCallback callback) {
		weakReference = new WeakReference<>(activity);
		mDownloadManager = (DownloadManager) weakReference.get().getSystemService(Context.DOWNLOAD_SERVICE);
		mDownLoadChangeObserver = new DownloadChangeObserver(new Handler());
		mDownloadReceiver = new DownloadReceiver();
		pathDir = Environment.DIRECTORY_DOWNLOADS;
		this.callback = callback;
		resume();
	}

	public void setUpdateListener(OnUpdateListener mUpdateListener) {
		this.mUpdateListener = mUpdateListener;
	}

	public void downloadAndInstallApk(final String apkUrl, final String title, final String desc) {

		if (!XXPermissions.isHasPermission(weakReference.get(), Permission.WRITE_EXTERNAL_STORAGE)) {
			XXPermissions.with(weakReference.get()).permission(Permission.WRITE_EXTERNAL_STORAGE).request(new OnPermission() {
				@Override
				public void hasPermission(List<String> granted, boolean isAll) {
					downloadApk(apkUrl, title, desc);
				}

				@Override
				public void noPermission(List<String> denied, boolean quick) {
					showError("未获得写文件授权");
				}
			});
		} else {
			downloadApk(apkUrl, title, desc);
		}
	}

	private void showError(String error) {
		if (weakReference.get() == null) {
			return;
		}

		Toast.makeText(weakReference.get(), error, Toast.LENGTH_SHORT).show();

	}

	private void downloadApk(String apkUrl, String title, String desc) {
		// fix bug : 装不了新版本，在下载之前应该删除已有文件
		apkFile = new File(weakReference.get().getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS), DOWNLOAD_FILE_NAME);


//        File apkFile = new File(pathDir,DOWNLOAD_FILE_NAME);
		if (apkFile != null && apkFile.exists()) {
			apkFile.delete();
		}

		DownloadManager.Request request = new DownloadManager.Request(Uri.parse(apkUrl));
		//设置title
		request.setTitle(title);
		// 设置描述
		request.setDescription(desc);
		// 完成后显示通知栏
		request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
		//这个文件是你的应用所专用的,软件卸载后，下载的文件将随着卸载全部被删除
		request.setDestinationInExternalFilesDir(weakReference.get(), Environment.DIRECTORY_DOWNLOADS, DOWNLOAD_FILE_NAME);

		//文件将存放在外部存储的确实download文件内，如果无此文件夹，创建之，如果有，下面将返回false。
		// * 系统有个下载文件夹，比如小米手机系统下载文件夹  SD卡--> Download文件夹
//        Environment.getExternalStoragePublicDirectory(pathDir).mkdir() ;
//        request.setDestinationInExternalPublicDir(pathDir, DOWNLOAD_FILE_NAME) ;

		//在手机SD卡上创建一个download文件夹
//         Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).mkdir() ;
		//指定下载到SD卡的/download/my/目录下
		// request.setDestinationInExternalPublicDir("/codoon/","codoon_health.apk");

		request.setMimeType("application/vnd.android.package-archive");
		//
		mReqId = mDownloadManager.enqueue(request);

	}

	/**
	 * 取消下载
	 */
	public void cancel() {
		if (mDownloadManager != null) {
			mDownloadManager.remove(mReqId);
		}
	}

	/**
	 * 对应 {@link Activity }
	 */
	public void resume() {
		//设置监听Uri.parse("content://downloads/my_downloads")

		weakReference.get().getContentResolver().registerContentObserver(Uri.parse("content://downloads/my_downloads"), true,
			mDownLoadChangeObserver);
		// 注册广播，监听APK是否下载完成
		weakReference.get().registerReceiver(mDownloadReceiver, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
	}

	/**
	 * 对应{@link Activity#onPause()} ()}
	 */
	public void onPause() {
		weakReference.get().getContentResolver().unregisterContentObserver(mDownLoadChangeObserver);
		weakReference.get().unregisterReceiver(mDownloadReceiver);
	}

	private void updateView() {
		int[] bytesAndStatus = new int[]{0, 0, 0};
		DownloadManager.Query query = new DownloadManager.Query().setFilterById(mReqId);
		Cursor c = null;
		try {
			c = mDownloadManager.query(query);
			if (c != null && c.moveToFirst()) {
				//已经下载的字节数
				bytesAndStatus[0] = c.getInt(c.getColumnIndexOrThrow(DownloadManager.COLUMN_BYTES_DOWNLOADED_SO_FAR));
				//总需下载的字节数
				bytesAndStatus[1] = c.getInt(c.getColumnIndexOrThrow(DownloadManager.COLUMN_TOTAL_SIZE_BYTES));
				//状态所在的列索引
				bytesAndStatus[2] = c.getInt(c.getColumnIndex(DownloadManager.COLUMN_STATUS));
			}
		} finally {
			if (c != null) {
				c.close();
			}
		}

		if (mUpdateListener != null) {
			mUpdateListener.update(bytesAndStatus[0], bytesAndStatus[1]);
		}

		Log.i(TAG, "下载进度：" + bytesAndStatus[0] + "/" + bytesAndStatus[1] + "");

	}

	class DownloadChangeObserver extends ContentObserver {

		/**
		 * Creates a content observer.
		 *
		 * @param handler The handler to run {@link #onChange} on, or null if none.
		 */
		public DownloadChangeObserver(Handler handler) {
			super(handler);
		}

		@Override
		public void onChange(boolean selfChange) {
			super.onChange(selfChange);
			updateView();
		}
	}


	class DownloadReceiver extends BroadcastReceiver {
		@Override
		public void onReceive(final Context context, final Intent intent) {
			boolean haveInstallPermission;
			// 兼容Android 8.0
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
				//先获取是否有安装未知来源应用的权限
				haveInstallPermission = context.getPackageManager().canRequestPackageInstalls();
				//没有权限
				if (!haveInstallPermission) {
					XXPermissions.with(weakReference.get()).permission(Permission.REQUEST_INSTALL_PACKAGES).request(new OnPermission() {
						@Override
						public void hasPermission(List<String> granted, boolean isAll) {
							installApk(context, intent);
						}

						@Override
						public void noPermission(List<String> denied, boolean quick) {
							showError("授权失败，无法安装应用");
						}
					});
				} else {
					installApk(context, intent);
				}
			} else {
				installApk(context, intent);
			}

		}
	}


	/**
	 * @param context
	 * @param intent
	 */
	private void installApk(Context context, Intent intent) {
		if (downloadCancel) {
			//取消下载
			return;
		}
		long completeDownLoadId = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1);

		Uri uri;
		Intent intentInstall = new Intent();
		intentInstall.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		intentInstall.setAction(Intent.ACTION_VIEW);

		if (completeDownLoadId == mReqId) {
			// 6.0以下
			if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
				uri = mDownloadManager.getUriForDownloadedFile(completeDownLoadId);
			} else if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
				// 6.0 - 7.0
				File apkFile = queryDownloadedApk(context, completeDownLoadId);
				uri = Uri.fromFile(apkFile);
			} else { // Android 7.0 以上
				PackageManager packageManager = context.getPackageManager();
				String packageName = context.getPackageName();
				uri = FileProvider.getUriForFile(context,
					packageName + ".fileprovider",
					apkFile);
				intentInstall.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
			}

			try {
				// 安装应用
				intentInstall.setDataAndType(uri, "application/vnd.android.package-archive");
				context.startActivity(intentInstall);
				this.callback.callback(Constants.INSTALL_START);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public void testInstall(Context context) {
		Intent intentInstall = new Intent();
		intentInstall.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		intentInstall.setAction(Intent.ACTION_VIEW);

		apkFile = new File(context.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS), DOWNLOAD_FILE_NAME);
		if (!apkFile.exists()) {
			showError("文件不存在");
			return;
		}
		String componentName = context.getPackageName();
//        showError("组件名称"+componentName + apkFile.getAbsolutePath());
//        return;
		Uri uri = FileProvider.getUriForFile(context,
			componentName + ".fileprovider",
			apkFile);
		intentInstall.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
		intentInstall.setDataAndType(uri, "application/vnd.android.package-archive");
		context.startActivity(intentInstall);
	}

	/***
	 *  通过downLoadId查询下载的apk，解决6.0以后安装的问题
	 */
	public static File queryDownloadedApk(Context context, long downloadId) {
		File targetApkFile = null;
		DownloadManager downloader = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);

		if (downloadId != -1) {
			DownloadManager.Query query = new DownloadManager.Query();
			query.setFilterById(downloadId);
			query.setFilterByStatus(DownloadManager.STATUS_SUCCESSFUL);
			Cursor cur = downloader.query(query);
			if (cur != null) {
				if (cur.moveToFirst()) {
					String uriString = cur.getString(cur.getColumnIndex(DownloadManager.COLUMN_LOCAL_URI));
					if (!TextUtils.isEmpty(uriString)) {
						targetApkFile = new File(Uri.parse(uriString).getPath());
					}
				}
				cur.close();
			}
		}
		return targetApkFile;
	}

	public interface OnUpdateListener {
		void update(int currentByte, int totalByte);
	}

}
