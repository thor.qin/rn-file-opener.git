'use strict';

import {
    NativeModules
} from 'react-native';

const {
    FileOpener
} = NativeModules;

export default FileOpener;