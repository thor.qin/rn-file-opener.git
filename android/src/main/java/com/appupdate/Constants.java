package com.appupdate;

/**
 * @author weifu.zhang
 */
public class Constants {
    public static final int DOWNLOAD_START = 0;
    public static final int DIALOG_CLOSE = -1;
    public static final int INSTALL_START = 2;

    public static boolean downloadCancel = false;
}
