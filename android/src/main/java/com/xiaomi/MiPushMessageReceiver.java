package com.xiaomi;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.WritableMap;
import com.fileopener.FileOpener;
import com.xiaomi.mipush.sdk.ErrorCode;
import com.xiaomi.mipush.sdk.MiPushClient;
import com.xiaomi.mipush.sdk.MiPushCommandMessage;
import com.xiaomi.mipush.sdk.MiPushMessage;
import com.xiaomi.mipush.sdk.PushMessageReceiver;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

/**
 *
 * @author weifu.zhang
 * @date 2018/1/17
 */

public class MiPushMessageReceiver extends PushMessageReceiver {
    private String mRegId;
    private String mTopic;
    private String mAlias;
    /// private String mAccount;
    // private String mStartTime;
    // private String mEndTime;

    @Override
    public void onReceivePassThroughMessage(Context context, MiPushMessage message) {
        /// Log.v(DemoApplication.TAG,
        // "onReceivePassThroughMessage is called. " + message.toString());
        // String log = context.getString(R.string.recv_passthrough_message,
        /// message.getContent());
        // MainActivity.logList.add(0, getSimpleDate() + " " + log)

        if (!TextUtils.isEmpty(message.getTopic())) {
            mTopic = message.getTopic();
        } else if (!TextUtils.isEmpty(message.getAlias())) {
            mAlias = message.getAlias();
        }

        // Message msg = Message.obtain();
        // msg.obj = log;
        // DemoApplication.getHandler().sendMessage(msg);
    }

    @Override
    public void onNotificationMessageClicked(Context context, MiPushMessage message) {
        Log.v("MIPUSH", "onNotificationMessageClicked is called. " + message.toString());

        /// Intent intent = new Intent();
        // intent.setData(Uri.parse("http://www.baidu.com"));
        // intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        // context.startActivity(intent);

        if (!TextUtils.isEmpty(message.getTopic())) {
            mTopic = message.getTopic();
        } else if (!TextUtils.isEmpty(message.getAlias())) {
            mAlias = message.getAlias();
        }
        //
        // Map<String, String> map = message.getExtra();
        // map.put("mTopic",mTopic);
        // map.put("mAlias",mAlias);
        // MultiplePush.extraMap = (Map)map;
        // MultiplePush.transmitNotificationOpen("XM");

        // Message msg = Message.obtain();
        // if (message.isNotified()) {
        // msg.obj = log;
        // }
        // DemoApplication.getHandler().sendMessage(msg);
        FileOpener.sendToClient("用户点击消息"+message.toString());
    }

    @Override
    public void onNotificationMessageArrived(Context context, MiPushMessage message) {
        /// Log.v(DemoApplication.TAG,
        // "onNotificationMessageArrived is called. " + message.toString());
        // String log = context.getString(R.string.arrive_notification_message,
        /// message.getContent());

        Log.v("MIPUSH", "onNotificationMessageArrived is called. " + message.toString());

        /// Intent intent = new Intent();
        // intent.setData(Uri.parse("http://www.baidu.com"));
        // context.startActivity(intent);

        if (!TextUtils.isEmpty(message.getTopic())) {
            mTopic = message.getTopic();
        } else if (!TextUtils.isEmpty(message.getAlias())) {
            mAlias = message.getAlias();
        }


        WritableMap map = Arguments.createMap();
        map.putString("message",message.toString());
        FileOpener.sendToClient("pushMsg",map);

        // Message msg = Message.obtain();
        // msg.obj = log;
        // DemoApplication.getHandler().sendMessage(msg);
    }

    @Override
    public void onCommandResult(Context context, MiPushCommandMessage message) {
        Logger.getLogger(this.getClass().getName()).info("onCommandResult is called. " + message.toString());
        String command = message.getCommand();
        List<String> arguments = message.getCommandArguments();
        String cmdArg1 = ((arguments != null && arguments.size() > 0) ? arguments.get(0) : null);
        String cmdArg2 = ((arguments != null && arguments.size() > 1) ? arguments.get(1) : null);
        String log;
        if (MiPushClient.COMMAND_REGISTER.equals(command)) {
            if (message.getResultCode() == ErrorCode.SUCCESS) {
                mRegId = cmdArg1;
                FileOpener.sendToClient("注册成功:"+mRegId);
                // MyApplication.getHandler().obtainMessage(0,"注册成功"+mRegId).sendToTarget();
                // log = context.getString(R.string.register_success);
            } else {
                FileOpener.sendToClient("注册失败:"+message.getReason());
                // MyApplication.getHandler().obtainMessage(0,"注册失败"+
                // message.getReason()).sendToTarget();
                // log = context.getString(R.string.register_fail);
            }
            /// } else if (MiPushClient.COMMAND_SET_ALIAS.equals(command)) {
            // if (message.getResultCode() == ErrorCode.SUCCESS) {
            // mAlias = cmdArg1;
            // log = context.getString(R.string.set_alias_success, mAlias);
            // } else {
            // log = context.getString(R.string.set_alias_fail, message.getReason());
            // }
            // } else if (MiPushClient.COMMAND_UNSET_ALIAS.equals(command)) {
            // if (message.getResultCode() == ErrorCode.SUCCESS) {
            // mAlias = cmdArg1;
            // log = context.getString(R.string.unset_alias_success, mAlias);
            // } else {
            // log = context.getString(R.string.unset_alias_fail, message.getReason());
            // }
            // } else if (MiPushClient.COMMAND_SET_ACCOUNT.equals(command)) {
            // if (message.getResultCode() == ErrorCode.SUCCESS) {
            // mAccount = cmdArg1;
            // log = context.getString(R.string.set_account_success, mAccount);
            // } else {
            // log = context.getString(R.string.set_account_fail, message.getReason());
            // }
            // } else if (MiPushClient.COMMAND_UNSET_ACCOUNT.equals(command)) {
            // if (message.getResultCode() == ErrorCode.SUCCESS) {
            // mAccount = cmdArg1;
            // log = context.getString(R.string.unset_account_success, mAccount);
            // } else {
            // log = context.getString(R.string.unset_account_fail, message.getReason());
            // }
            // } else if (MiPushClient.COMMAND_SUBSCRIBE_TOPIC.equals(command)) {
            // if (message.getResultCode() == ErrorCode.SUCCESS) {
            // mTopic = cmdArg1;
            // log = context.getString(R.string.subscribe_topic_success, mTopic);
            // } else {
            // log = context.getString(R.string.subscribe_topic_fail, message.getReason());
            // }
            // } else if (MiPushClient.COMMAND_UNSUBSCRIBE_TOPIC.equals(command)) {
            // if (message.getResultCode() == ErrorCode.SUCCESS) {
            // mTopic = cmdArg1;
            // log = context.getString(R.string.unsubscribe_topic_success, mTopic);
            // } else {
            // log = context.getString(R.string.unsubscribe_topic_fail,
            /// message.getReason());
            // }
            // } else if (MiPushClient.COMMAND_SET_ACCEPT_TIME.equals(command)) {
            // if (message.getResultCode() == ErrorCode.SUCCESS) {
            // mStartTime = cmdArg1;
            // mEndTime = cmdArg2;
            // log = context.getString(R.string.set_accept_time_success, mStartTime,
            /// mEndTime);
            // } else {
            // log = context.getString(R.string.set_accept_time_fail, message.getReason());
            // }
        } else {
            log = message.getReason();
            FileOpener.sendToClient("未知CommandResult"+message.getCommand() + log);
            // MyApplication.getHandler().obtainMessage(0,"未知CommandResult ：
            // "+log).sendToTarget();
        }
        // MainActivity.logList.add(0, getSimpleDate() + " " + log);

        // Message msg = Message.obtain();
        // msg.obj = log;
        // DemoApplication.getHandler().sendMessage(msg);
    }

    @Override
    public void onReceiveRegisterResult(Context context, MiPushCommandMessage message) {
        // Log.v(DemoApplication.TAG,
        // "onReceiveRegisterResult is called. " + message.toString());
        Logger.getLogger(this.getClass().getName()).info("onReceiveRegisterResult is called. " + message.toString());
        String command = message.getCommand();
        List<String> arguments = message.getCommandArguments();
        String cmdArg1 = ((arguments != null && arguments.size() > 0) ? arguments.get(0) : null);
        // String log;
        if (MiPushClient.COMMAND_REGISTER.equals(command)) {

            if (message.getResultCode() == ErrorCode.SUCCESS) {
                mRegId = cmdArg1;
                // MultiplePush.transmitReceiveRegistrationId(mRegId);
                // MyApplication.getHandler().obtainMessage(0, "注册成功"+mRegId) . sendToTarget();
                // log = context.getString(R.string.register_success);
                FileOpener.sendToClient("注册成功--:"+mRegId);
            } else {
                FileOpener.sendToClient("注册失败:"+message.getReason());
                // MyApplication.getHandler().obtainMessage(0, "注册失败"+ messag
                // e.getReason()).sendToTarget();
                // log = context.getString(R.string.register_fail);
            }
        } else {
            FileOpener.sendToClient("未知命令:"+message.getReason());
            // MyApplication.getHandler().obtainMessage(0, "未知命令" +
            // message.getReason()).sendToTarget();
        }

        // Message msg = Message.obtain();
        // msg.obj = log;
        // DemoApplication.getHandler().sendMessage(msg);
    }

    @SuppressLint("SimpleDateFormat")
    private static String getSimpleDate() {
        return new SimpleDateFormat("MM-dd hh:mm:ss").format(new Date());
    }
}
