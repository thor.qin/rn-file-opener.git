package com.huawei.android.hms.agent.push;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.WritableMap;
import com.fileopener.FileOpener;
import com.huawei.hms.support.api.push.PushReceiver;

public class HuaweiMessageReceiver extends PushReceiver {
    public static final String TAG = "HuaweiPushRevicer";

    public static final String ACTION_UPDATEUI = "action.updateUI";

    public static String token;

    @Override
    public void onToken(Context context, String tokenIn, Bundle extras) {
        String belongId = extras.getString("belongId");
        token = tokenIn;

        WritableMap map = Arguments.createMap();
        map.putString("belongId",belongId);
        map.putString("token",tokenIn);
        FileOpener.sendToClient("onToken",map);

//        MultiplePush.transmitReceiveRegistrationId(token);
        // MyApplication.getHandler().obtainMessage(0,"注册成功，token为： "+token).sendToTarget();
    }

    @Override
    public boolean onPushMsg(Context context, byte[] msg, Bundle bundle) {
        try {
            //CP可以自己解析消息内容，然后做相应的处理
            String content = new String(msg, "UTF-8");
////            Intent intent = new Intent();
////            intent.setAction(ACTION_UPDATEUI);
////            intent.putExtra("log", "收到PUSH透传消息,消息内容为:" + content);
////            context.sendBroadcast(intent);
            FileOpener.sendToClient("收到PUSH透传消息,消息内容为:" + content);
        } catch (Exception e) {
            Log.d(TAG,"======");
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public void onEvent(Context context, Event event, Bundle extras) {
        if (Event.NOTIFICATION_OPENED.equals(event) || Event.NOTIFICATION_CLICK_BTN.equals(event)) {
            int notifyId = extras.getInt(BOUND_KEY.pushNotifyId, 0);
            // Intent intent = new Intent();
            // intent.setAction(ACTION_UPDATEUI);
            // intent.putExtra("log", "收到通知栏消息点击事件,notifyId:" + notifyId);
            // context.sendBroadcast(intent);
            if (0 != notifyId) {
                NotificationManager manager = (NotificationManager) context
                        .getSystemService(Context.NOTIFICATION_SERVICE);
                manager.cancel(notifyId);
            }
        }

        String message = extras.getString(BOUND_KEY.pushMsgKey);

        WritableMap map = Arguments.createMap();
        map.putString("message",message);
        FileOpener.sendToClient("pushMsg",map);

    }

    @Override
    public void onPushState(Context context, boolean pushState) {
        //如果push断开，需要做响应的处理，后期优化
//        Intent intent = new Intent();
//        intent.setAction(ACTION_UPDATEUI);
//        intent.putExtra("log", "Push连接状态为:" + pushState);
//        context.sendBroadcast(intent);


        WritableMap map = Arguments.createMap();
        map.putBoolean("pushState",pushState);
        FileOpener.sendToClient("pushState",map);
    }
}