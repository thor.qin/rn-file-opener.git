## 使用
<code>
import FileOpener from 'rn-file-opener';

......

componentDidMount() {
    DeviceEventEmitter.addListener('inited', (e: Event) => {
      // handle event.
      this.setState({
        initState: "已经初始化！" + JSON.stringify(e)
      })
    });
    
    DeviceEventEmitter.addListener('onToken', (e: Event) => {
      // handle event.
      this.setState({
        tokenState: "Token信息：" + JSON.stringify(e)
      })
    });

    DeviceEventEmitter.addListener('pushMsg', (e: Event) => {
      // handle event.
      this.setState({
        messageInfo: "message信息：" + JSON.stringify(e)
      })
    });

    DeviceEventEmitter.addListener('pushState', (e: Event) => {
      // handle event.
      this.setState({
        pushState: "pushState改变：" + JSON.stringify(e)
      })
    });

    DeviceEventEmitter.addListener('debug', (e: Event) => {
      // handle event.
      this.setState({
        debug: "debug信息：" + JSON.stringify(e)
      })
    });

    FileOpener.notifyJSDidLoad((d) => {
      // alert("服务器返回" + d);
    })
  }



</code>

## 关于PUSH的额外说明
- 非华为小米的Android和所有iOS的PUSH，走极光通道
    - 调用华为小米的PUSH消息API，发送的消息类型为自定义消息类型（PUSH带action的）
    - 华为小米的appID和Key信息再gradle文件里查找配置com.mi.appid等
    - Android 的MainActivity加如下代码：
    <pre><code> 
    void handlePushMessage(android.content.Intent intent) {
        if (intent.hasExtra("PUSH")) {
            String value = intent.getStringExtra("PUSH");
            FileOpener.sendToClient(value);
            Log.d("===MAIN===", value);
        }
    }

    @Override
    public void onNewIntent(android.content.Intent intent) {
        handlePushMessage(intent);
    } 
    </code></pre>


## 关于热更新的代码
- Android 部分主要代码如下
  </pre><code>

 private static final String bundleFile  = "index.android.bundle";

 private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {
     ......
        @Nullable
        @Override
        protected String getJSBundleFile() {
            String path = String.format("%s%s%s",Environment.getDataDirectory() , File.separator ,bundleFile);
            if(new File(path).exists()){
                return path;
            }
            return super.getJSBundleFile();
        }
        
        @Override
        protected String getBundleAssetName(){
            return bundleFile;
        }

  </code></pre>

  - iOS 部分主要代码如下

    <code>
RCTRootView *rootView = [[RCTRootView alloc] initWithBundleURL:jsCodeLocation moduleName:@"app_name" initialProperties:nil launchOptions:launchOptions]; 
    </code>


## 关于升级
### Android 升级  
- appName应用名称、
- releaseNode 更新说明、
- url 升级地址、
- cancelable 是否可以取消（强制升级使用）
- 
<code>
public void showUpdate(final String appName,String releaseNode,  final String url,boolean cancelable) 
</code>

### iOS 升级
- 使用Link直接打开浏览器升级，如果直接升级，浏览器地址就对应下载的plist文件地址
  
<code>
var url = 'http://tsd.com/d.plist';
     Linking.openURL(url)
     .catch((err)=>{
       console.log('An error occurred', err);
     });
</code>